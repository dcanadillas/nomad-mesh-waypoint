variable "image" {
  default = "hcdcanadillas/pydemo-front"
}
variable "image_version" {
  default = "v1.2-amd64"
}
variable "datacenter" {
  default = "dc1"
}

job "front" {
  datacenters = ["${var.datacenter}"]

  group "front" {
    network {
      port "front" {}
      mode = "bridge"
    }
    service {
      name = "frontend"
      tags = ["frontend", "python", "${var.image_version}"]
      port = "front"
      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "backend"
              local_bind_port = 9090
            }
          }
        }
      }
    }
    update {
      canary = 1
      max_parallel = 1
      auto_promote = false
    }

    task "frontend" {
      driver = "docker"

      config {
        image = "${var.image}:${var.image_version}"
        ports = ["front"]
        force_pull = true
      }
      env {
        PORT = "$${NOMAD_PORT_front}"
        BACKEND_URL = "http://localhost:9090"
      }
    }
  }
}