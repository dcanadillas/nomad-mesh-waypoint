project = "pyfront-demo"
runner {
  enabled = true
}

variable "nomad_dc" {
  default = "dc1"
}

variable "nomad_job" {
  default = "front"
}

variable "arch" {
  # default = "amd64"
}

variable "runner" {
  # default = ""
}

runner {
  enabled = true
  profile = var.runner
}

app "front" {
  path = "${path.project}"
  url {
    auto_hostname = false
  }
  
  build {
    use "docker" {}
    registry {
      use "docker" {
        image = "hcdcanadillas/bk8s-front"
        tag   = "v1-${var.arch}"
        # local = true
      }
    }
  }

  deploy {
    # use "nomad" {
    #   datacenter = var.nomad_dc
    #   static_environment = {
    #     PORT = "8080"
    #     BACKEND_URL = "http://$$NOMAD:9090"
    #   }
    #   service_port = 8080
    #   replicas = 1
    # }
    use "nomad-jobspec" {
      jobspec = templatefile("${path.app}/${var.nomad_job}.nomad.tpl",{
        datacenter = var.nomad_dc
      })
    }
  }
}

 
