project = "app-demo-nomad"

variable "namespace" {
  default = "default"
}
variable "client_constraint" {
  type = object({
    backend_host = string
    frontend_host = string
  })
}
variable "nomad_dc" {
  default = "dc1"
}

variable "cloud" {
  default = "gcp"
}

variable "front_name" {
  default = "front"
}

variable "back_name" {
  default = "back"
} 

variable "front_version" {
  # default = "v1.2-amd64"
}
variable "back_version" {
  # default = "v1.2-amd64"
}

variable "docker_user" {
  # default = ""
}

variable "docker_token" {
  # default = ""
}

variable "registry" {
  default = "hcdcanadillas"
}

variable "consul_addr" {}

runner {
  enabled = true
}

pipeline "consul_config" {
  step "service_defaults" {
    image_url = "curlimages/curl"
    use "exec"{
      command = "sh"
      args = [
        "-c",
        "curl -XPUT ${var.consul_addr}/v1/config -d \"{\\\"Kind\\\": \\\"service-defaults\\\", \\\"Name\\\": \\\"backend\\\", \\\"Protocol\\\": \\\"http\\\"}\" && curl -XPUT ${var.consul_addr}/v1/config -d \"{\\\"Kind\\\": \\\"service-defaults\\\", \\\"Name\\\": \\\"frontend\\\", \\\"Protocol\\\": \\\"http\\\"}\""
      ]
    }
  }
  // step "back_intention" {
  //   image_url = "hashicorp/consul"
  //   use "exec"{
  //     command = "sh"
  //     args = [
  //       "-c",
  //       "curl -L https://gitlab.com/api/v4/projects/37980990/repository/files/consul%2Fback-intention.hcl/raw\\?ref\\=master -o intention.hcl && cat intention.hcl"
  //     ]
  //   }
  // }
  step "back_intention" {
    image_url = "curlimages/curl"
    use "exec"{
      command = "sh"
      args = [
        "-c",
        "curl -H \"X-Consul-Token: $CONSUL_HTTP_TOKEN\" -XPUT ${var.consul_addr}/v1/config -d \"{\\\"Kind\\\": \\\"service-intentions\\\",\\\"Name\\\": \\\"backend\\\",\\\"Sources\\\": [{\\\"Action\\\": \\\"allow\\\",\\\"Name\\\": \\\"frontend\\\"}]}\""
      ]
    }
  }
}

pipeline "full_deploy" {
  step "build" {
    use build {}
  }
  step "consul_config" {
    use "pipeline" {
      name = "consul_config"
      project = "app-demo-nomad"
    }
  }
  step "deploy" {
    use "deploy" {}
  }
}

app "front" {
  path = "${path.project}/front"
  url {
    auto_hostname = false
  }
  
  build {
    use "docker" {}
    // use docker-pull {
    //   image = "hcdcanadillas/pydemo-front"
    //   tag   = var.front_version
    //   // auth {
    //   //   username = var.docker_user
    //   //   password = var.docker_token
    //   // }
    // }
    registry {
      use "docker" {
        image = "${var.registry}/pydemo-front"
        tag   = var.front_version
        // local = true
        auth {
          username = var.docker_user
          password = var.docker_token
        }
      }
    }
  }

  deploy {
    use "nomad-jobspec" {
      jobspec = templatefile("${path.project}/templates/${var.front_name}.nomad.tpl",{
        datacenter = var.nomad_dc
        namespace = var.namespace
        cloud = var.cloud
        client_constraint = var.client_constraint.frontend_host,
      })
    }
  }
}

app "back" {
  path = "${path.project}/back"
  url {
    auto_hostname = false
  }

  build {
    use "docker" {}
    // use docker-pull {
    //   image = "hcdcanadillas/pydemo-back"
    //   tag   = var.back_version
    //   // disable_entrypoint = true
    // }
    registry {
      use "docker" {
        image = "${var.registry}/pydemo-back"
        tag   = var.back_version
        // local = true
        auth {
          username = var.docker_user
          password = var.docker_token
        }
      }
    }
  }

  deploy {
    use "nomad-jobspec" {
      jobspec = templatefile("${path.project}/templates/${var.back_name}.nomad.tpl",{
        datacenter = var.nomad_dc,
        namespace = var.namespace,
        client_constraint = var.client_constraint.backend_host,
        image = artifact.image
        tag = artifact.tag
        cloud = var.cloud
      })
    }
  }
}