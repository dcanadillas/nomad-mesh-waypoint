
job "back" {
  datacenters = ["${datacenter}"]

  namespace = "${namespace}"

  group "back" {
    count = 1
    constraint {
      operator  = "regexp"
      attribute = "$${node.unique.name}"
      value     = "${client_constraint}"
    }
    network {
      port "back" {
        static = 9090
        to = 9090
      }
      # To use Consul Connect use "bridge" mode, but it is not supported in MacOS
      mode = "bridge"
    }
    service {
      name = "backend"
      tags = ["backend", "python"]
      meta {
        %{ if cloud == "gcp" }
        zone = "$${attr.platform.gce.zone}"
        %{ endif }
        %{ if cloud == "azure" }
        location = "$${attr.platform.azure.location}"
        azure_vm = "$${attr.unique.platform.azure.name}"
        scale-set = "$${attr.platform.azure.scale-set}"
        resource_group = "$${attr.platform.azure.resource-group}"
        %{ endif }
        %{ if cloud == "aws" }
        ami_id = "$${attr.platform.aws.ami-id}"
        %{ endif }
        hostname = "$${attr.unique.hostname}"
      }
      port = 9090
      connect {
        sidecar_service {}
      }
    }

    # consul {
    #   namespace = var.consul_namespace
    # }


    task "back" {
      driver = "docker"

      config {
        image = "${image}:${tag}"
        ports = ["back"]
        force_pull = true
      }
      env {
        // For URL service
        PORT = "$$${NOMAD_PORT_back}"
      }
    }
  }
}