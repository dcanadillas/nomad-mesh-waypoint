Kind          = "service-resolver"
Name          = "frontend"
DefaultSubset = "v1"
Subsets = {
  v1 = {
    Filter = "\"v1.2-amd64\" in Service.Tags"
  }
  v2 = {
    Filter = "\"v1.3\" in Service.Tags"
  }
}
